package com.example.unicornapp.user.service;

import com.example.unicornapp.user.model.Role;
import com.example.unicornapp.user.model.Users;
import com.example.unicornapp.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user =userRepository.findByUsername(username);
        if(user == null){
            log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found in the database");
        }else {
            log.info("User found in the database");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().name()));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),authorities        );
    }


    public Users saveUser(Users user) {
        if(userRepository.findByUsername(user.getUsername())==null){
            log.info("Save new user {} to the db", user.getUsername());

            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(user);
        }
        log.info("User {} already exists in the db", user.getUsername());
        return null;
    }


    public Users getUser(String username) {
        log.info("Get user to the db");
        return userRepository.findByUsername(username);
    }

    public List<Users> getUsers() {
        log.info("Get all users to the db");
        return userRepository.findAll();
    }


}
