package com.example.unicornapp.user.model;


public enum Role {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN
}
