package com.example.unicornapp;

import com.example.unicornapp.user.model.Role;
import com.example.unicornapp.user.model.Users;
import com.example.unicornapp.user.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class UnicornappApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnicornappApplication.class, args);
    }

    @Bean
    CommandLineRunner run(UserService userService){
        return args -> {
            if(userService.getUser("yuliia") == null){
                userService.saveUser(new Users(null, "Yuliia Skorbach", "yuliia", "1234", Role.ROLE_USER));
            }
        };
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

}
